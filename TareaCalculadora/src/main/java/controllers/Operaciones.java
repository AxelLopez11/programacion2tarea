/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import views.ComponentsFrame;
/**
 *
 * @author Jarvin
 */
public class Operaciones{
    private double n;
    private double Fat;
    private int CF;
    private int CF2;

    public int getCF2() {
        return CF2;
    }

    public void setCF2(int CF2) {
        this.CF2 = CF2;
    }
   
    public int getCF() {
        return CF;
    }

    public void setCF(int CF) {
        this.CF = CF;
    }
    
    public double getFat() {
        return Fat;
    }

    public void setFat(double Fat) {
        this.Fat = Fat;
    }
    

    public Operaciones() {
    }

    public double getN() {
        return n;
    }

    public void setN(double n) {
        this.n = n;
    }

    public String Operacion() {
        int cont = 0;
        String Result="";
        for (int i = 1; i <= n; i++) {
            if (n%i==0) {
                cont++;
            }
            if (cont!=2) {
                Result="No es primo";
            }else{
                Result="Es primo";
            }
        }
        
        return Result;
    }
    
    public double Factorial(){
        double res;
        double factorial=1;
        double numA = Fat;
						
	while(Fat != 0){
        	factorial *= Fat;
		Fat--;
            }    
        return factorial;
    }
    
    public int SerieFib(){
        int a = 0, b = 1, c;
        for (int j = 0; j < n; j++) {
            c = a + b;
            a = b;
            b = c;
	}
        return a;
    }
    
    
    public int SerieF2(){
        int m = 1, k = 1, l;
        
        for (int j = 0; j < CF2; j++) {
            System.out.println(m);
            l = m + k;
            m = k;
            k = l;
        }
        return m;
    }
}
