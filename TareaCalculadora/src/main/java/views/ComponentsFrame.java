/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;
import java.awt.Button;
import java.awt.Label;
import java.awt.TextField;
import controllers.Operaciones;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;
import javax.swing.*;
import java.awt.TextArea;



/**
 *commit
 * @author Jarvin
 */
public class ComponentsFrame extends wFrame {
    private static final long serialVersionUID = 5475809701447035232L;
	private Label label1, label2, label3, label4, label5;
	private TextField textField1, textField2, textField3, textField4, textField5;
	private Button button1;
        private TextArea textA;
	
        public ComponentsFrame(MainFrame intance) {
		super(intance);
		initComponents();
	}
        
	public void initComponents() {
		setSize(350,600);
		setTitle("Calculadora");
		setLayout(null);
		
		label1 = new Label("Ingrese un numero");
		label1.setBounds(20, 50, 150, 32);
		add(label1);
		
		textField1 = new TextField();
		textField1.setBounds(20, 85, 50, 32);
		add(textField1);
		
		button1 = new Button("Calcular");
		button1.setBounds(100,85,100,32);
		add(button1);
                
                label2 = new Label("Es Primo: ");
		label2.setBounds(20, 130, 150, 32);
		add(label2);
                
                textField2 = new TextField();
		textField2.setBounds(20, 165, 100, 32);
		add(textField2);
                
                label3 = new Label("Factorial: ");
		label3.setBounds(20, 210, 150, 32);
		add(label3);
                
                textField3 = new TextField();
                textField3.setBounds(20, 250, 100, 32);
                add(textField3);
                
                label4 = new Label("Enésimo término de la serie fibonacci: ");
		label4.setBounds(20, 300, 220, 32);
		add(label4);
                
                textField4 = new TextField();
                textField4.setBounds(20, 340, 100, 32);
                add(textField4);
                
                label5 = new Label("Serie fibonacci desde 1 hasta enésimo término: ");
                label5.setBounds(20, 390, 280, 32);
                add(label5);
                
                textA = new TextArea();
                textA.setBounds(20, 430, 280, 150);
                add(textA);
                
                
                button1.addMouseListener(new MouseAdapter() {
                    public void mousePressed(MouseEvent e) {
                        if (textField1.getText().equals("")) {
                            JOptionPane.showMessageDialog(label1, "Campos vacios");
                            textField1.requestFocusInWindow();
                        }else{
                            Operaciones op = new Operaciones();
                            double n;
                            double F;
                            int SF;
                            int SF2;
                            
                            SF= Integer.parseInt(textField1.getText());
                            n = Double.parseDouble(textField1.getText());
                            F = Double.parseDouble(textField1.getText());
                            SF2 = Integer.parseInt(textField1.getText());
                            op.setCF(SF);
                            op.setFat(F);
                            op.setN(n);
                            op.setCF2(SF2);
                            textField2.setText(String.valueOf(op.Operacion()));
                            op.Operacion();
                            textField3.setText(String.valueOf(op.Factorial()));
                            op.Factorial();
                            textField4.setText(String.valueOf(op.SerieFib()));
                            op.SerieFib();
                            //textA.setText(String.valueOf(op.SerieF2()));
                            //op.SerieF2();
                            }
				
                        }
                });                                  
                
                   //textField2.setText(string);
               
                
            }
}
                    

