/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;
import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import models.FormInterface;
/**
 *
 * @author Jarvin
 */
public class MainFrame extends Frame implements FormInterface{
    private static final long serialVersionUID = -2483114542263271239L;
	private Button button1;
        private Label Tittle;
	private MainFrame intance;
	public MainFrame() {
		initComponents();
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dispose();
				System.exit(0);
			}
		});
	}
	@Override
	public void initComponents() {
		setLayout(null);
		setSize(390,200);
		setResizable(false);
                
                Tittle = new Label("Calculadora");
                Tittle.setBounds(165, 50, 100, 32);
                add(Tittle);
                
		intance = this;
		button1 = new Button("Ingresar");
		button1.setBounds(150,100,100,32);
		add(button1);
		
		button1.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				ComponentsFrame cf = new ComponentsFrame(intance);
				cf.showForm();
			}
		});
		
	}
	
	public void showForm() {
		setVisible(true);
		setLocationRelativeTo(null);
		toFront();
	}
	
	public void clean() {
		// TODO Auto-generated method stub
		
	}
	public void showForm(boolean maximize) {

	}

    
}
