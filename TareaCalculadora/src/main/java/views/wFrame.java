/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import models.FormInterface;
/**
 *Eclipse
 * @author Jarvin
 */
public class wFrame extends Frame implements FormInterface{
        private static final long serialVersionUID = -3044516451727926394L;
	Frame parentFrame;
	
	public wFrame(MainFrame parentFrame) {
		this.parentFrame = parentFrame;
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if(parentFrame!=null) {
					parentFrame.setVisible(true);
				}
				dispose();
			}
		});
	}
	@Override
	public void initComponents() {
		
	}

	@Override
	public void showForm() {
		if(parentFrame!=null) {
			parentFrame.setVisible(false);
		}
		setVisible(true);
		setLocationRelativeTo(null);
		toFront();
		
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showForm(boolean maximize) {
		// TODO Auto-generated method stub
		
	}
    
}
